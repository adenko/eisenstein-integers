module Data.Eisenstein where 

import Control.Applicative ( Applicative(liftA2) ) 
import Control.Monad ( join ) 
import Data.Complex ( Complex(..) ) 
import Data.Semiring 
import Prelude hiding ((*),(+),(-), negate)
import qualified Prelude as P 

-- | Eisenstein integers are complex numbers of the form (r, w*omega), where omega is the principal cube root of unity. 
-- They form a triangular lattice in the complex plane. 
data Eisenstein x = Eisenstein x x deriving Functor 

instance Applicative Eisenstein where 
    pure = join Eisenstein 
    (<*>) (Eisenstein f g) (Eisenstein x y) = Eisenstein (f x) (g y)

instance Ring x => Semiring (Eisenstein x) where 
    one = Eisenstein one zero 
    zero = Eisenstein zero zero 
    plus = liftA2 plus 
    times (Eisenstein r w) (Eisenstein r' w') = Eisenstein (r * r' - w * w') (w * r' + r * w' - w * w') 
instance Ring x => Ring (Eisenstein x) where 
    negate (Eisenstein r w) = Eisenstein (negate r) (negate w)

-- | Embeds the Eisenstein integers in the complex numbers in the canonical way
eisensteinToComplex :: RealFloat x => Eisenstein x -> Complex x 
eisensteinToComplex (Eisenstein r w) = (r :+ 0) P.+ (w :+ 0) P.* root where root = (P.negate 0.5) :+ ((sqrt 3.0) P./ 2.0)

-- | The Eisenstein integers are a Euclidean domain 
eisensteinNorm :: Ring x => Eisenstein x -> x 
eisensteinNorm (Eisenstein r w) = r * r - r * w + w * w 